package br.com.meualuguel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MeualuguelApiApplication {

	
	
	public static void main(String[] args) {
		SpringApplication.run(MeualuguelApiApplication.class, args);
	}

}
