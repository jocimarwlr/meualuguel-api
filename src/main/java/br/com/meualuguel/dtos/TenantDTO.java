package br.com.meualuguel.dtos;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

import jakarta.persistence.Column;
import lombok.Data;

@Data
public class TenantDTO {
	
	
	private String name;
	
	private String personalIdCpf;
	
	private String phoneNumber;
	
	private String personalIdRg;
	
	private String email;
	
	private Integer houseNumber;
	
	private BigDecimal rent;
	
	private LocalDate invoiceDate;

	private Boolean smsNote;
	
	private Boolean whatsNote;
	
	private Boolean emailNote;

}
