package br.com.meualuguel.dtos;

import java.math.BigDecimal;
import java.time.LocalDate;

import lombok.Data;

@Data
public class PaymentDTO {
	
	

	
	private BigDecimal amountPaid;
	
	private Integer tenantId;
	
	private LocalDate paymentDate;
	
	
	

}
