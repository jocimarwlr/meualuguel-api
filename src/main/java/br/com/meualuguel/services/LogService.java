package br.com.meualuguel.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.meualuguel.model.Log;
import br.com.meualuguel.repositories.LogRepository;

@Service
public class LogService {

	
	@Autowired
	LogRepository logRepository;
	
	public Log saveLog(Log log) {
		
		return logRepository.save(log);
		
	}
	
}
