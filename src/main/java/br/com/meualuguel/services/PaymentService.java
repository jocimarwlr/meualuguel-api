package br.com.meualuguel.services;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.meualuguel.dtos.PaymentDTO;
import br.com.meualuguel.model.Payment;
import br.com.meualuguel.model.Tenant;
import br.com.meualuguel.repositories.PaymentRepository;

@Service
public class PaymentService {

	@Autowired
	PaymentRepository paymentRepository;
	
	@Autowired
	TenantService tenantService;
	
	
	public Payment makePayment(PaymentDTO paymentDTO) {
		
		Payment payment = new Payment(paymentDTO);
		Tenant tenant = tenantService.findOneTenant(paymentDTO.getTenantId());
		
		payment.setTenant(tenant);
		
		
		return    paymentRepository.save(payment);
		}


	public List<Payment> findlastPayments() {
		 // Obtém a data atual
        LocalDate currentDate = LocalDate.now();
        
        // Obtém o primeiro dia do mês corrente
        LocalDate firstDayOfMonth = currentDate.withDayOfMonth(1);
        
        // Obtém o último dia do mês corrente
        LocalDate lastDayOfMonth = currentDate.withDayOfMonth(currentDate.lengthOfMonth());

        // Busca os pagamentos dentro do intervalo do mês corrente
        List<Payment> payments =  paymentRepository.findByPaymentDateBetween(firstDayOfMonth, lastDayOfMonth);
        
        for (Payment payment : payments) {
            payment.populateNameAndHouseNumber();
        }

        return payments;
	}


	public void removePayment(Integer paymentId) {

		Payment payment = findPayment(paymentId);
		
		if(payment!=null) {
			paymentRepository.delete(payment);
		}
		
		
		
	}


	private Payment findPayment(Integer paymentId) {
		
		Optional<Payment> payment =  paymentRepository.findById(paymentId);
		
		if(payment.isPresent()) {
			
			return payment.get();
			
		}else {
			return null;
		}
	}
	
}
