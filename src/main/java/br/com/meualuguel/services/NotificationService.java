package br.com.meualuguel.services;
import java.time.LocalDate;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.meualuguel.model.Payment;
import br.com.meualuguel.model.Tenant;
import br.com.meualuguel.repositories.TenantRepository;

@Service
public class NotificationService {
	
	
	@Autowired
	TenantRepository tenantRepository;

	private final RestTemplate restTemplate;
    private final ObjectMapper objectMapper;

    @Autowired
    public NotificationService(RestTemplate restTemplate, ObjectMapper objectMapper) {
        this.restTemplate = restTemplate;
        this.objectMapper = objectMapper;
    }

    public void sendNotification(String ownerToken, String title, String body) {
        // Endpoint da API de notificação
        String notificationEndpoint = "https://inovaestudios.com.br/notification-api/send-notification";

        // Montar o objeto para o corpo da requisição
        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put("title", title);
        requestBody.put("body", body);
        requestBody.put("tokens", Collections.singletonList(ownerToken)); // tokens como lista de uma única string

        // Converter o requestBody para JSON
        String jsonBody;
        try {
            jsonBody = objectMapper.writeValueAsString(requestBody);
        } catch (JsonProcessingException e) {
            System.err.println("Erro ao converter requestBody para JSON: " + e.getMessage());
            return;
        }

        // Configurar o cabeçalho da requisição
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        // Criar a entidade de requisição com o corpo e cabeçalho
        HttpEntity<String> requestEntity = new HttpEntity<>(jsonBody, headers);

        // Enviar a requisição POST para o endpoint de notificação
        ResponseEntity<String> response = restTemplate.exchange(notificationEndpoint, HttpMethod.POST, requestEntity, String.class);

        // Verificar o status da resposta, se necessário
        if (response.getStatusCode() == HttpStatus.OK) {
            System.out.println("Notificação enviada com sucesso!");
        } else {
            System.out.println("Falha ao enviar notificação. Código de status: " + response.getStatusCodeValue());
        }
    }
    
    

	public void verificarAluguelVencendoHoje() {
		LocalDate dataAtual = LocalDate.now();
		int diaAtual = dataAtual.getDayOfMonth();

	    
	    List<Tenant> tenants = tenantRepository.findAll(); // Suponha que haja um repository para Tenant
	    
	    for (Tenant tenant : tenants) {
	        if (tenant.getInvoiceDate().getDayOfMonth()==diaAtual && dataAtual.isAfter(tenant.getInvoiceDate().plusDays(1))) {
	            // Vencimento hoje, agora verificamos os pagamentos
	            boolean pagamentoNoMes = verificarPagamentoNoMes(tenant, dataAtual.getMonthValue(), dataAtual.getYear());
	            
	            if (!pagamentoNoMes) {
	                enviarNotificacao(tenant);
	            }
	        }
	    }
	}

	
	private boolean verificarPagamentoNoMes(Tenant tenant, int mes, int ano) {
	    List<Payment> payments = tenant.getPayments();
	    
	    for (Payment payment : payments) {
	        LocalDate dataPagamento = payment.getPaymentDate();
	        
	        if (dataPagamento.getMonthValue() == mes && dataPagamento.getYear() == ano) {
	            return true; // Pagamento encontrado para o mês atual
	        }
	    }
	    
	    return false; // Nenhum pagamento encontrado para o mês atual
	}

	
	private void enviarNotificacao(Tenant tenant) {
	    // Suponha que haja uma classe de serviço para chamar a API de notificação
	    String ownerToken = tenant.getOwner().getFcmToken();
	    String title = "Vencimento de aluguel";
	    String body = tenant.getName() + ", Valor do aluguel - " + tenant.getRent();
	    
	    // Chamar o serviço de notificação
	    sendNotification(ownerToken, title, body);
	}

	
}
