package br.com.meualuguel.services;

import java.util.List;
import java.util.Optional;

import org.hibernate.ObjectNotFoundException;
import org.hibernate.annotations.TenantId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.meualuguel.dtos.TenantDTO;
import br.com.meualuguel.model.Owner;
import br.com.meualuguel.model.Tenant;
import br.com.meualuguel.repositories.TenantRepository;

@Service
public class TenantService {
	
	
	@Autowired
	TenantRepository tenantRepository;
	@Autowired
	OwnerService ownerService;
	
	
	public List<Tenant> findAllTenants(){
		
		
		return tenantRepository.findAll();
		
		
	}
	
	
	
	
	public Tenant saveTenant(TenantDTO tenantDTO,String userId) throws Exception {
		
		//user is the Owner of properties
		Owner owner = ownerService.findOwner(userId);
		if(owner==null){
			
			return null;
		}
		Tenant tenant = new Tenant(tenantDTO);
		tenant.setOwner(owner);
		
		return tenantRepository.save(tenant);
		
		
		
	}
	
	
	
	public Tenant findOneTenant(Integer tenantId)  {
		
		Optional<Tenant> tenant = tenantRepository.findById(tenantId);
		
		return tenant.orElseThrow(()-> new ObjectNotFoundException(Tenant.class,tenant.toString() ));
		
		
	}

}
