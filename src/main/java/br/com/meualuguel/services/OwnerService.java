package br.com.meualuguel.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.meualuguel.dtos.OwnerDTO;
import br.com.meualuguel.model.Owner;
import br.com.meualuguel.repositories.OwnerRepository;

@Service
public class OwnerService {
	
	
	@Autowired
	OwnerRepository ownerRepository;
	
	
	
	public Owner findOwner(String userId) throws Exception {
		
		Optional<Owner> owner = ownerRepository.findByUserId(userId);
		 
		
		if(owner.isPresent()) {
			return owner.get();
		}else {
			return  null;
		}
	}



	public Owner saveNewOwner(OwnerDTO ownerDTO) throws Exception {
		
		// analisar se Owner ja existe
		
		Owner ownerGravado = findOwner(ownerDTO.getUserId());
		Owner owner = new Owner(ownerDTO);
		
		if(ownerGravado!=null && ownerGravado.getFcmToken().equals(ownerDTO.getFcmToken())) {
			
			return ownerGravado;
		}else if(ownerGravado!=null && !ownerGravado.getFcmToken().equals(ownerDTO.getFcmToken())) {
		
			owner.setId(ownerGravado.getId());
			
			
		}
		
		
		
		return  ownerRepository.save(owner);
	}


	

	public Owner findById(Integer userId) throws Exception {
		
		 Optional<Owner> owner= ownerRepository.findById(userId);
		 
		 return owner.orElseThrow(()->new Exception());
		 
	}

}
