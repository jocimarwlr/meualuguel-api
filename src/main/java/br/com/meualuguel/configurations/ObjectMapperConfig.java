package br.com.meualuguel.configurations;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ObjectMapperConfig {

    @Bean
    public ObjectMapper objectMapper() {
    	   ObjectMapper objectMapper = new ObjectMapper();

           // Registro do módulo para suporte a tipos de data/tempo do Java 8
           objectMapper.registerModule(new JavaTimeModule());

           // Configuração para incluir propriedades não nulas ao serializar
           objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

           // Configuração para ignorar propriedades desconhecidas em JSON
           objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

           // Configuração para aceitar datas no formato ISO-8601
           objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

           return objectMapper;
    }
}

