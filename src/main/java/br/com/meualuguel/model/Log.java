package br.com.meualuguel.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Data
@Table(name = "logs")
public class Log implements Serializable   {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name="ip_address")
	private String ip;
	@Column(name="coordinates")
	private String coordinates;
	@Column(name="operating_system")
	private String operatingSystem;
	@Column(name = "user_id")
	private Integer userId;
	@Column(name = "device_name")
	private String deviceName;
	@Column(name = "network_id")
	private String networkId;
	@Column(name="log_date")
	private LocalDate logDate = LocalDate.now();
	@Column(name="network_details", columnDefinition = "LONGTEXT")
	private String networkDetails;
}
