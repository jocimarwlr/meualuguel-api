package br.com.meualuguel.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import org.hibernate.annotations.ManyToAny;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.meualuguel.dtos.PaymentDTO;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Entity
@Data
@Table(name = "payments")
public class Payment implements Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@NotNull
	@Column(name = "amount_paid")
	private BigDecimal amountPaid;
	
	@Column(name = "payment_date")
	private LocalDate paymentDate= LocalDate.now();
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "tenant_id", referencedColumnName = "id")
	private Tenant tenant;
	
	@Transient
	private String nameAndHouseNumber;
	

	public Payment(PaymentDTO paymentDTO) {
		this.amountPaid=paymentDTO.getAmountPaid();
		if (paymentDTO.getPaymentDate()!=null) {
			this.paymentDate=paymentDTO.getPaymentDate();
		}
		
	}

	
	public Payment() {
		
	}
	
	
	// Método para preencher o atributo nameAndHouseNumber
    public void populateNameAndHouseNumber() {
        if (tenant != null) {
            this.nameAndHouseNumber = tenant.getName() + " - " + tenant.getHouseNumber();
        }
    }

}
