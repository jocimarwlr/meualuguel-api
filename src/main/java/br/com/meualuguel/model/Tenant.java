package br.com.meualuguel.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.meualuguel.dtos.TenantDTO;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "tenants")
@Data
public class Tenant {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name="name")
    private String name;
    @Column(name = "cpf") 
    private String personalIdCpf;
    @Column(name = "phone")
    private String phoneNumber;
    @Column(name = "rg")
    private String personalIdRg;
    @Column(name = "email")
    private String email;
    @Column(name="house_number")
    private Integer houseNumber;
    @Column(name = "rent")
    private BigDecimal rent;
    @Column(name = "invoice_date")
    private LocalDate invoiceDate;
    @Column(name = "sms_note")
    private Boolean smsNote;
    @Column(name = "whatsapp_note")
    private Boolean whatsNote;
    @Column(name = "email_note")
    private Boolean emailNote;
    
    
    
    @OneToMany(mappedBy = "tenant",cascade = CascadeType.ALL)
    private List<Payment> payments = new ArrayList<>();
    
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "owner_id",referencedColumnName = "id")
    private Owner owner;
    
    
    
    public Tenant(TenantDTO tenantDTO) {
        this.name = tenantDTO.getName();
        this.personalIdCpf = tenantDTO.getPersonalIdCpf();
        this.phoneNumber = tenantDTO.getPhoneNumber();
        this.personalIdRg = tenantDTO.getPersonalIdRg();
        this.email = tenantDTO.getEmail();
        this.houseNumber = tenantDTO.getHouseNumber();
        this.rent = tenantDTO.getRent();
        this.invoiceDate =tenantDTO.getInvoiceDate();
        this.smsNote = tenantDTO.getSmsNote();
        this.whatsNote = tenantDTO.getWhatsNote();
        this.emailNote = tenantDTO.getEmailNote();
    }
    
    
    
    
    
    public Tenant() {
        // Construtor padrão
    }
}
