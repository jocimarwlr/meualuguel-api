package br.com.meualuguel.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import br.com.meualuguel.dtos.OwnerDTO;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "tb_owner")
@Data

public class Owner {
	
	





	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "name")
	private String name;
	@Column(name = "fcm_token")
	private String fcmToken;
	@Column(name = "user_id")
	private String userId;
	@Column(name = "register_date")
    private LocalDate registerDate= LocalDate.now();
	
	public Owner(OwnerDTO ownerDTO) {
		this.name=ownerDTO.getName();
		this.fcmToken=ownerDTO.getFcmToken();
		this.userId=ownerDTO.getUserId();
	}
	  
	
	public Owner() {
		// TODO Auto-generated constructor stub
	}
	
	
	@OneToMany(mappedBy = "owner")
	private List<Tenant> tenants = new ArrayList<>();
	
	
	
}
