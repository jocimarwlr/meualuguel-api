package br.com.meualuguel.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.meualuguel.model.Log;

@Repository
public interface LogRepository extends JpaRepository<Log, Integer> {

}
