package br.com.meualuguel.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.meualuguel.model.Owner;

public interface OwnerRepository extends JpaRepository<Owner, Integer> {

	Optional<Owner> findByUserId(String userId);

}
