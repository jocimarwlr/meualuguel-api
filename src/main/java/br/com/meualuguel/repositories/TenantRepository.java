package br.com.meualuguel.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.meualuguel.model.Tenant;

@Repository
public interface TenantRepository extends JpaRepository<Tenant, Integer> {

}
