package br.com.meualuguel.controllers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.meualuguel.services.NotificationService;

@RestController
@RequestMapping("/notification")
public class NotificationController {

    private final NotificationService notificationService;

    @Autowired
    public NotificationController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @GetMapping("/vencimentos")
    public ResponseEntity<String> checkAndNotifyRentDue() {
        try {
            notificationService.verificarAluguelVencendoHoje();
            return ResponseEntity.ok("Verificação de aluguéis e notificações executadas com sucesso.");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Erro ao iniciar verificação e notificações: " + e.getMessage());
        }
    }
}
