package br.com.meualuguel.controllers;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.meualuguel.dtos.OwnerDTO;
import br.com.meualuguel.model.Owner;
import br.com.meualuguel.services.OwnerService;

@RestController
@RequestMapping("owners")
@CrossOrigin("*")
public class OwnerController {
	
	
	@Autowired
	OwnerService ownerService;
	
	
	@PostMapping
	public ResponseEntity<?> saveNewUser(@RequestBody OwnerDTO ownerDTO){
		
		System.out.println("Gravar new User.....");
		
		
		try {
	        // Convertendo OwnerDTO para Owner
	        Owner savedOwner = ownerService.saveNewOwner(ownerDTO);

	        // Construindo URI do recurso criado
	        URI uriComponents = ServletUriComponentsBuilder.fromCurrentRequest()
	                                .path("/{id}")
	                                .buildAndExpand(savedOwner.getId())
	                                .toUri();

	        return ResponseEntity.created(uriComponents).build();
	        
	    } catch (Exception e) {
	        // Caso ocorra uma exceção, retornar ResponseEntity com status 500 Internal Server Error
	        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
	                             .body("Erro ao salvar proprietário: " + e.getMessage());
	    }
		
		
	}
	
	
	
	
	@GetMapping("/{userId}")
	public ResponseEntity<?> findOne(@PathVariable Integer userId) throws Exception{
		
		Owner owner = ownerService.findById(userId);
		
		return ResponseEntity.ok().body(owner);
		
	}
	
	
	
	

}
