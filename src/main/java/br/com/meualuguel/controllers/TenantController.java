package br.com.meualuguel.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.meualuguel.dtos.TenantDTO;
import br.com.meualuguel.model.Log;
import br.com.meualuguel.model.Tenant;
import br.com.meualuguel.services.LogService;
import br.com.meualuguel.services.TenantService;
import jakarta.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("tenants")
@CrossOrigin("*")
public class TenantController {
	
	
	
	
	@Autowired
	TenantService tenantService;
	
	@Autowired
	LogService logService;
	
	
	@GetMapping
	public ResponseEntity<List<Tenant>> findAllTenants(){
		
		
		return ResponseEntity.ok().body(tenantService.findAllTenants());
	}
	
	
	@PostMapping
	public ResponseEntity<Tenant> saveTenant(@RequestParam(name = "owner") String userId , @RequestBody  TenantDTO tenantDTO, HttpServletRequest req) throws Exception{
		
		// Ler dados do cabeçalho
        String latitude = req.getHeader("latitude");
        String longitude = req.getHeader("longitude");
        String ipAddress = req.getHeader("ipAddress");
        String deviceName = req.getHeader("deviceName");
        String osName = req.getHeader("osName");
        String osVersion = req.getHeader("osVersion");
        String detalhesRede = req.getHeader("detalhesRede");
        
       
        
        Log log = new Log();
        log.setCoordinates(latitude + "," + longitude);
        log.setIp(ipAddress);
        log.setDeviceName(deviceName);
        log.setOperatingSystem(osName + "," + "Version :" + osVersion );
        log.setNetworkDetails(detalhesRede);
        
        logService.saveLog(log);
        
        
        
        
        // Verificar se os cabeçalhos não são nulos antes de usar seus valores
        if (latitude != null && longitude != null && ipAddress != null) {
            // Faça o que precisar com esses dados do cabeçalho
        	System.out.println("Latitude :" + latitude);
        	System.out.println("Longitude :" + longitude);
        	System.out.println("Endereço IP publico :" + ipAddress);
        	System.out.println("Device name :" + deviceName);
        	System.out.println("Operating system :" + osName);
        	System.out.println("System version :" + osVersion);
        } else {
            // Lidar com a ausência de dados do cabeçalho
            System.out.println("Alguns cabeçalhos não foram passados na requisição.");
        }
        
        // Salvar o inquilino
		
		
		Tenant tenant = tenantService.saveTenant(tenantDTO,userId);
		
		return ResponseEntity.ok().body(tenant);
		
		
		
	}
	
	

}
