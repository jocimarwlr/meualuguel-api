package br.com.meualuguel.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.meualuguel.dtos.PaymentDTO;
import br.com.meualuguel.model.Payment;
import br.com.meualuguel.services.PaymentService;

@RestController
@CrossOrigin("*")
@RequestMapping("/payments")
public class PaymentController {
	
	
	@Autowired
	PaymentService paymentService;
	
	
	@PostMapping
	public ResponseEntity<Payment> savePayment(@RequestBody PaymentDTO paymentDTO){
		
		Payment pePayment = paymentService.makePayment(paymentDTO);
		
		return ResponseEntity.ok().body(pePayment);
		
	}
	
	
	
	@GetMapping
	public ResponseEntity<List<Payment>> lastPaymentsMade(){
		
		List<Payment> lastpayments = paymentService.findlastPayments();
		
		
		return ResponseEntity.ok().body(lastpayments);
		
		
	}
	
	

  @DeleteMapping("/{paymentId}")
  
  public ResponseEntity<HttpStatus> removePayment(@PathVariable Integer paymentId){
	  
	  
	  paymentService.removePayment(paymentId);
	  
	  return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	  
	  
	  
  }
	
	
	
	
	
	

}
